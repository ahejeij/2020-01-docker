#!/bin/sh

if [ $# -lt 1 ] ; then
	echo "Usage: ./start.sh <PORT>"
	exit 1
fi

TEST_PORT=$1

sudo docker run -p $TEST_PORT:80 -v ./files:/usr/share/nginx/html:ro -d nginx
